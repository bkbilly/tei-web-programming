SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS `webindex` ;
CREATE SCHEMA IF NOT EXISTS `webindex` DEFAULT CHARACTER SET utf8 ;
USE `webindex` ;

-- -----------------------------------------------------
-- Table `webindex`.`User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webindex`.`User` ;

CREATE  TABLE IF NOT EXISTS `webindex`.`User` (
  `UserName` VARCHAR(15) NOT NULL ,
  `Salt` INT UNSIGNED NULL ,
  `Password` VARCHAR(35) NOT NULL ,
  `eMail` VARCHAR(45) NOT NULL ,
  `VerifiedNum` INT UNSIGNED NULL ,
  `Verified` TINYINT(1) NULL DEFAULT false ,
  PRIMARY KEY (`UserName`) ,
  UNIQUE INDEX `UserName_UNIQUE` (`UserName` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `webindex`.`BusinessType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webindex`.`BusinessType` ;

CREATE  TABLE IF NOT EXISTS `webindex`.`BusinessType` (
  `BusinessType` VARCHAR(40) NOT NULL ,
  PRIMARY KEY (`BusinessType`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `webindex`.`Business`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webindex`.`Business` ;

CREATE  TABLE IF NOT EXISTS `webindex`.`Business` (
  `BusinessTitle` VARCHAR(40) NOT NULL ,
  `User_Name` VARCHAR(15) NOT NULL ,
  `Business_Type` VARCHAR(25) NOT NULL ,
  `Address` VARCHAR(35) NOT NULL ,
  `PostalCode` INT UNSIGNED NOT NULL ,
  `PhoneNumber` VARCHAR(15) NOT NULL ,
  `KeyWords` VARCHAR(45) NULL ,
  `WebAddress` VARCHAR(25) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NULL ,
  `Default_IMG` VARCHAR(45) NOT NULL DEFAULT 'Undentified.png' ,
  `Description` VARCHAR(450) NULL ,
  PRIMARY KEY (`BusinessTitle`) ,
  INDEX `fk_Business_User1` (`User_Name` ASC) ,
  INDEX `fk_Business_BusinessType1` (`Business_Type` ASC) ,
  CONSTRAINT `fk_Business_User1`
    FOREIGN KEY (`User_Name` )
    REFERENCES `webindex`.`User` (`UserName` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Business_BusinessType1`
    FOREIGN KEY (`Business_Type` )
    REFERENCES `webindex`.`BusinessType` (`BusinessType` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `webindex`.`Image`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `webindex`.`Image` ;

CREATE  TABLE IF NOT EXISTS `webindex`.`Image` (
  `ImageID` INT NOT NULL AUTO_INCREMENT ,
  `Name` VARCHAR(40) NOT NULL ,
  `ALT` VARCHAR(45) NULL ,
  `Business_Title` VARCHAR(40) NOT NULL ,
  INDEX `fk_BusinessImages_Business1` (`Business_Title` ASC) ,
  PRIMARY KEY (`ImageID`) ,
  UNIQUE INDEX `NameID_UNIQUE` (`ImageID` ASC) ,
  CONSTRAINT `fk_BusinessImages_Business1`
    FOREIGN KEY (`Business_Title` )
    REFERENCES `webindex`.`Business` (`BusinessTitle` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `webindex`.`User`
-- -----------------------------------------------------
START TRANSACTION;
USE `webindex`;
INSERT INTO `webindex`.`User` (`UserName`, `Salt`, `Password`, `eMail`, `VerifiedNum`, `Verified`) VALUES ('bkbilly', 54167, '54e3tvsmnd4Fg', 'bkbilly@hotmail.com', 30538, 1);
INSERT INTO `webindex`.`User` (`UserName`, `Salt`, `Password`, `eMail`, `VerifiedNum`, `Verified`) VALUES ('sakis', 24213, '24588dKLcTNJA', 'tzogas1991@gmail.com', 19591, 1);
INSERT INTO `webindex`.`User` (`UserName`, `Salt`, `Password`, `eMail`, `VerifiedNum`, `Verified`) VALUES ('test3', 82262, '82mGEKhRFshLU', 'mail@mail.com', 59589, 1);
INSERT INTO `webindex`.`User` (`UserName`, `Salt`, `Password`, `eMail`, `VerifiedNum`, `Verified`) VALUES ('test2', 18473, '1808709m/Qd/o', 'mail@mail.com', 60454, 1);
INSERT INTO `webindex`.`User` (`UserName`, `Salt`, `Password`, `eMail`, `VerifiedNum`, `Verified`) VALUES ('test1', 78354, '780kKp47a5OqU', 'mail@mail.com', 70919, 1);
INSERT INTO `webindex`.`User` (`UserName`, `Salt`, `Password`, `eMail`, `VerifiedNum`, `Verified`) VALUES ('test4', 73072, '73KLMxVc5LtsY', 'mail@mail.com', 50685, 1);
INSERT INTO `webindex`.`User` (`UserName`, `Salt`, `Password`, `eMail`, `VerifiedNum`, `Verified`) VALUES ('test5', 73157, '73KLMxVc5LtsY', 'mail@mail.com', 50122, 1);
INSERT INTO `webindex`.`User` (`UserName`, `Salt`, `Password`, `eMail`, `VerifiedNum`, `Verified`) VALUES ('test10', 75478, '7520ysy5pDXZA', 'mail@mail.com', 26064, 1);
INSERT INTO `webindex`.`User` (`UserName`, `Salt`, `Password`, `eMail`, `VerifiedNum`, `Verified`) VALUES ('test11', 16058, '16ubO6XZETYYY', 'mail@mail.com', 99645, 1);
INSERT INTO `webindex`.`User` (`UserName`, `Salt`, `Password`, `eMail`, `VerifiedNum`, `Verified`) VALUES ('test12', 40443, '40vkiBAXOAIdM', 'mail@mail.com', 60331, 1);
INSERT INTO `webindex`.`User` (`UserName`, `Salt`, `Password`, `eMail`, `VerifiedNum`, `Verified`) VALUES ('test13', 43955, '43SFcl6HDxjJs', 'mail@mail.com', 28391, 1);
INSERT INTO `webindex`.`User` (`UserName`, `Salt`, `Password`, `eMail`, `VerifiedNum`, `Verified`) VALUES ('test6', 74712, '743GIouc1VKdI', 'mail@mail.com', 31132, 1);
INSERT INTO `webindex`.`User` (`UserName`, `Salt`, `Password`, `eMail`, `VerifiedNum`, `Verified`) VALUES ('test7', 39053, '39sfcCEdHjEhY', 'mail@mail.com', 57908, 1);
INSERT INTO `webindex`.`User` (`UserName`, `Salt`, `Password`, `eMail`, `VerifiedNum`, `Verified`) VALUES ('test8', 55129, '55s6HaZxwY/vg', 'mail@mail.com', 70633, 1);
INSERT INTO `webindex`.`User` (`UserName`, `Salt`, `Password`, `eMail`, `VerifiedNum`, `Verified`) VALUES ('test9', 43587, '43SFcl6HDxjJs', 'mail@mail.com', 19212, 1);

COMMIT;

-- -----------------------------------------------------
-- Data for table `webindex`.`BusinessType`
-- -----------------------------------------------------
START TRANSACTION;
USE `webindex`;
INSERT INTO `webindex`.`BusinessType` (`BusinessType`) VALUES ('Mobile');
INSERT INTO `webindex`.`BusinessType` (`BusinessType`) VALUES ('Alcohol');
INSERT INTO `webindex`.`BusinessType` (`BusinessType`) VALUES ('Pet Shop');
INSERT INTO `webindex`.`BusinessType` (`BusinessType`) VALUES ('Restaurant');
INSERT INTO `webindex`.`BusinessType` (`BusinessType`) VALUES ('CandyShop');
INSERT INTO `webindex`.`BusinessType` (`BusinessType`) VALUES ('Car Service');
INSERT INTO `webindex`.`BusinessType` (`BusinessType`) VALUES ('Hotel');
INSERT INTO `webindex`.`BusinessType` (`BusinessType`) VALUES ('Hospital');
INSERT INTO `webindex`.`BusinessType` (`BusinessType`) VALUES ('SuperMarket');
INSERT INTO `webindex`.`BusinessType` (`BusinessType`) VALUES ('GameStore');
INSERT INTO `webindex`.`BusinessType` (`BusinessType`) VALUES ('ComputerStore');
INSERT INTO `webindex`.`BusinessType` (`BusinessType`) VALUES ('Cafeteria');

COMMIT;

-- -----------------------------------------------------
-- Data for table `webindex`.`Business`
-- -----------------------------------------------------
START TRANSACTION;
USE `webindex`;
INSERT INTO `webindex`.`Business` (`BusinessTitle`, `User_Name`, `Business_Type`, `Address`, `PostalCode`, `PhoneNumber`, `KeyWords`, `WebAddress`, `Default_IMG`, `Description`) VALUES ('Amstel', 'sakis', 'Alcohol', 'Everywhere', 158789, '8001568934', 'beer, amstel', 'www.amstel.com', 'Amstel.jpg', 'Amstel Brewery is a Dutch brewery founded in 1870 on the Mauritskade in Amsterdam, The Netherlands. It was taken over by Heineken International in 1968, and the brewing plant closed down in 1982, with production moving to the main Heineken plant at Zoeterwoude.');
INSERT INTO `webindex`.`Business` (`BusinessTitle`, `User_Name`, `Business_Type`, `Address`, `PostalCode`, `PhoneNumber`, `KeyWords`, `WebAddress`, `Default_IMG`, `Description`) VALUES ('Carrefour', 'test7', 'SuperMarket', 'Ionias 15', 65980, '27410245679', 'Carrefur,supermarket', 'www.carrefour.gr', 'Carrefour.jpg', 'Carrefour S.A. is an international hypermarket chain headquartered in Boulogne Billancourt,France, in Greater Paris. It is one of the largest hypermarket chains in the world.');
INSERT INTO `webindex`.`Business` (`BusinessTitle`, `User_Name`, `Business_Type`, `Address`, `PostalCode`, `PhoneNumber`, `KeyWords`, `WebAddress`, `Default_IMG`, `Description`) VALUES ('E.A', 'test10', 'GameStore', 'Karaiskaki 1', 26790, '2109675500', 'E.A,Game', 'www.ea.com', 'EA-Logo.jpg', 'Electronic Arts, Inc. (EA) is a major American developer, marketer, publisher and distributor of video games. Founded and incorporated on May 28, 1982 by Trip Hawkins, the company was a pioneer of the early home computergames industry and was notable for promoting the designers and programmers responsible for its games. It is one of the largest video game publishers in the world.');
INSERT INTO `webindex`.`Business` (`BusinessTitle`, `User_Name`, `Business_Type`, `Address`, `PostalCode`, `PhoneNumber`, `KeyWords`, `WebAddress`, `Default_IMG`, `Description`) VALUES ('Ferrari', 'test12', 'Car Service', 'Mpotsari 45', 256777, '2106599647', 'Ferrari, car', 'www.ferrari.com', 'Ferrari.jpg', 'Ferrari is an Italian sports car manufacturer based in Maranello, Italy. Founded by Enzo Ferrari in 1929, as Scuderia Ferrari, the company sponsored drivers and manufactured race cars before moving into production of street-legal vehicles as Ferrari S.p.A. in 1947. Throughout its history, the company has been noted for its continued participation in racing, especially inFormula One.');
INSERT INTO `webindex`.`Business` (`BusinessTitle`, `User_Name`, `Business_Type`, `Address`, `PostalCode`, `PhoneNumber`, `KeyWords`, `WebAddress`, `Default_IMG`, `Description`) VALUES ('Flocafe', 'test13', 'Cafeteria', 'protopapadaki 6', 41221, '2410251830', 'Flocafe', 'www.flocafe.gr', 'Flocafe.jpg', 'Flocafe is a Greek franchise coffee house chain and belongs to Vivartia, a nutrition conglomerate brand that owns several similar franchises. It opened its first outlet in Athens in 1994 and now operates over 70 coffee houses in Greece, Cyprus and on several cruise ships. In 2006 it held a 10.2% of the Greek market share.');
INSERT INTO `webindex`.`Business` (`BusinessTitle`, `User_Name`, `Business_Type`, `Address`, `PostalCode`, `PhoneNumber`, `KeyWords`, `WebAddress`, `Default_IMG`, `Description`) VALUES ('Hilton', 'test6', 'Hotel', 'Papanikolaou 56', 679854, '210-76905', 'Hilton, Hotel', 'www.hiltonathens.gr', 'Hilton.jpg', 'Hilton Hotels and Resorts (formerly known as Hilton Hotels) is an international hotel chain which includes many luxury hotelsand resorts as well as select service hotels. It was founded by Conrad Hilton and is now owned by Hilton Worldwide. Hilton hotels are either owned by, managed by, or franchised to independent operators by Hilton Worldwide. ');
INSERT INTO `webindex`.`Business` (`BusinessTitle`, `User_Name`, `Business_Type`, `Address`, `PostalCode`, `PhoneNumber`, `KeyWords`, `WebAddress`, `Default_IMG`, `Description`) VALUES ('Johnnie Walker', 'test11', 'Alcohol', 'Euterpis 16', 15756, '+806521578932', 'Whiskey', 'www.johnniewalker.com', 'JohnnieWalker.jpg', 'Johnnie Walker is a brand of Scotch Whisky owned by Diageo and originated in Kilmarnock, Ayrshire, Scotland. It is the most widely distributed brand of blended Scotch whisky in the world, sold in almost every country with yearly sales of over 130 million bottles.');
INSERT INTO `webindex`.`Business` (`BusinessTitle`, `User_Name`, `Business_Type`, `Address`, `PostalCode`, `PhoneNumber`, `KeyWords`, `WebAddress`, `Default_IMG`, `Description`) VALUES ('Nokia', 'test8', 'Mobile', 'papaioanou 12', 78900, '21179800', 'Nokia, mobile', 'www.nokia.com', 'Nokia.jpg', 'Nokia was the world\'\'s largest vendor of mobile phones from 1998 to 2012. However over the past five years it has suffered declining market share as a result of the growing use of smartphones, principally the Apple iPhone and devices running onGoogle\'\'s Android platform.');
INSERT INTO `webindex`.`Business` (`BusinessTitle`, `User_Name`, `Business_Type`, `Address`, `PostalCode`, `PhoneNumber`, `KeyWords`, `WebAddress`, `Default_IMG`, `Description`) VALUES ('Opel', 'test3', 'Car Service', 'Anastaseos 42', 15669, '210-654789', 'Opel, car', 'www.Opel.com', 'Opel.jpg', 'Opel is a German automobile company founded by Adam Opel in 1862. Opel has been building automobiles since 1899, and became an Aktiengesellschaft in 1929. The company is headquartered in Russelsheim, Germany. It became a majority-stake subsidiary of the General Motors Corporation in 1929 and has been a wholly owned subsidiary since 1931.');
INSERT INTO `webindex`.`Business` (`BusinessTitle`, `User_Name`, `Business_Type`, `Address`, `PostalCode`, `PhoneNumber`, `KeyWords`, `WebAddress`, `Default_IMG`, `Description`) VALUES ('Plaisio', 'test4', 'ComputerStore', 'E.Benizelou 65', 567892, '210-46700', 'Plaiso,pc,compouter', 'www.plaisio.gr', 'Plaisio.jpg', 'Plaisio Computers is one of the largest computer and technology retailers in Greece. Expanding from the original Plaisio office and art supply business, Plaisio Computers first appeared in the market in 1999-2000 and soon expanded with its franchise to cover most of the major cities in Greece. ');
INSERT INTO `webindex`.`Business` (`BusinessTitle`, `User_Name`, `Business_Type`, `Address`, `PostalCode`, `PhoneNumber`, `KeyWords`, `WebAddress`, `Default_IMG`, `Description`) VALUES ('Samsung', 'test9', 'Mobile', 'leoforos kifisias 28', 15232, '2157485487', 'samsung, tv, mobile', 'www.samsung.com', 'Samsung.jpg', 'Samsung Group is a South Korean multinational conglomerate company headquartered in Samsung Town, Seoul. It comprises numerous subsidiaries and affiliated businesses, most of them united under the Samsung brand, and is the largest South Korean chaebol.');
INSERT INTO `webindex`.`Business` (`BusinessTitle`, `User_Name`, `Business_Type`, `Address`, `PostalCode`, `PhoneNumber`, `KeyWords`, `WebAddress`, `Default_IMG`, `Description`) VALUES ('Starbucks', 'test1', 'Cafeteria', 'Mesogeion 155', 15878, '21065487925', 'Starbucks, Caffe', 'www.starbucksi.gr', 'Starbucks.gif', 'Starbucks Corporation is an international coffee company and coffeehouse chain based in Seattle, Washington. Starbucks is the largest coffeehouse company in the world, with 19,555 stores in 58 countries, including 12,811 in the United States, 1,248 in Canada, 965 in Japan, 766 in Great Britain, 580 in China and 420 in South Korea.');
INSERT INTO `webindex`.`Business` (`BusinessTitle`, `User_Name`, `Business_Type`, `Address`, `PostalCode`, `PhoneNumber`, `KeyWords`, `WebAddress`, `Default_IMG`, `Description`) VALUES ('SweatWorld', 'test2', 'CandyShop', 'Ailodios 16', 20300, '27440-28134', 'SweatWorld, Candy', 'www.SweatWorld.gr', 'SweetWorld.png', 'A confectionery store (more commonly referred to as a sweet shop in the United Kingdom, a candy store in the North America, or a lolly shop[1] in Australia) sells confectionery and is usually targeted to children. Most confectionery stores are filled with an assortment of sweets far larger than a grocer or convenience store could accommodate. ');
INSERT INTO `webindex`.`Business` (`BusinessTitle`, `User_Name`, `Business_Type`, `Address`, `PostalCode`, `PhoneNumber`, `KeyWords`, `WebAddress`, `Default_IMG`, `Description`) VALUES ('Wind', 'bkbilly', 'Mobile', 'Athens', 156879, '21065489876', 'wind, mobile', 'www.wind.com.gr', 'Wind.png', 'WIND Hellas, formerly STET Hellas, is an integrated telecommunications provider with headquarters in Athens, Greece. WIND is the 3rd largest mobile operator in Greece (after Cosmote and Vodafone) with more than 4.4 million active subscribers (September 2010)');
INSERT INTO `webindex`.`Business` (`BusinessTitle`, `User_Name`, `Business_Type`, `Address`, `PostalCode`, `PhoneNumber`, `KeyWords`, `WebAddress`, `Default_IMG`, `Description`) VALUES ('X-box', 'test5', 'GameStore', 'U.S.A', 456347, '+02356754328', 'X-box, Games', 'www.xbox.com', 'XBOX Live.jpg', 'The Xbox Live service is available as both a free and subscription-based service, known as Xbox Live Free and Xbox Live Goldrespectively, with several features such as online gaming restricted to the Gold service. Prior to October 2010, the free service was known as Xbox Live Silver.');

COMMIT;

-- -----------------------------------------------------
-- Data for table `webindex`.`Image`
-- -----------------------------------------------------
START TRANSACTION;
USE `webindex`;
INSERT INTO `webindex`.`Image` (`ImageID`, `Name`, `ALT`, `Business_Title`) VALUES (1, 'Starbucks.gif', 'image/gif', 'Starbucks');
INSERT INTO `webindex`.`Image` (`ImageID`, `Name`, `ALT`, `Business_Title`) VALUES (2, 'Wind.png', 'image/png', 'Wind');
INSERT INTO `webindex`.`Image` (`ImageID`, `Name`, `ALT`, `Business_Title`) VALUES (3, 'Amstel.jpg', 'image/jpeg', 'Amstel');
INSERT INTO `webindex`.`Image` (`ImageID`, `Name`, `ALT`, `Business_Title`) VALUES (4, 'SweetWorld.png', 'image/png', 'SweatWorld');
INSERT INTO `webindex`.`Image` (`ImageID`, `Name`, `ALT`, `Business_Title`) VALUES (5, 'Opel.jpg', 'image/jpeg', 'Opel');
INSERT INTO `webindex`.`Image` (`ImageID`, `Name`, `ALT`, `Business_Title`) VALUES (6, 'Plaisio.jpg', 'image/jpeg', 'Plaisio');
INSERT INTO `webindex`.`Image` (`ImageID`, `Name`, `ALT`, `Business_Title`) VALUES (7, 'XBOX Live.jpg', 'image/jpeg', 'X-box');
INSERT INTO `webindex`.`Image` (`ImageID`, `Name`, `ALT`, `Business_Title`) VALUES (8, 'Hilton.jpg', 'image/jpeg', 'Hilton');
INSERT INTO `webindex`.`Image` (`ImageID`, `Name`, `ALT`, `Business_Title`) VALUES (9, 'Carrefour.jpg', 'image/jpeg', 'Carrefour');
INSERT INTO `webindex`.`Image` (`ImageID`, `Name`, `ALT`, `Business_Title`) VALUES (10, 'Samsung.jpg', 'image/jpeg', 'Samsung');
INSERT INTO `webindex`.`Image` (`ImageID`, `Name`, `ALT`, `Business_Title`) VALUES (11, 'JohnnieWalker.jpg', 'image/jpeg', 'Johnnie Walker');
INSERT INTO `webindex`.`Image` (`ImageID`, `Name`, `ALT`, `Business_Title`) VALUES (12, 'EA-Logo.jpg', 'image/jpeg', 'E.A');
INSERT INTO `webindex`.`Image` (`ImageID`, `Name`, `ALT`, `Business_Title`) VALUES (13, 'Nokia.jpg', 'image/jpeg', 'Nokia');
INSERT INTO `webindex`.`Image` (`ImageID`, `Name`, `ALT`, `Business_Title`) VALUES (14, 'nokia-5610.jpg', 'image/jpeg', 'Nokia');
INSERT INTO `webindex`.`Image` (`ImageID`, `Name`, `ALT`, `Business_Title`) VALUES (15, 'nokia-5800-xpressmusic-2.jpg', 'image/jpeg', 'Nokia');
INSERT INTO `webindex`.`Image` (`ImageID`, `Name`, `ALT`, `Business_Title`) VALUES (16, 'Ferrari.jpg', 'image/jpeg', 'Ferrari');
INSERT INTO `webindex`.`Image` (`ImageID`, `Name`, `ALT`, `Business_Title`) VALUES (17, 'FerrariCar.jpg', 'image/jpeg', 'Ferrari');
INSERT INTO `webindex`.`Image` (`ImageID`, `Name`, `ALT`, `Business_Title`) VALUES (18, 'Flocafe.jpg', 'image/jpeg', 'Flocafe');

COMMIT;
