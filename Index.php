<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html version="-//W3C//DTD XHTML 1.1//EN" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>webINDEX</title>
		<link href="Properties.css" rel="stylesheet" type="text/css" />
		<link href="" rel="stylesheet" type="text/css" id="StyleOptions"/>
		<script type="text/javascript" src="Load.js"></script>
		<script type="text/javascript" src="Forms.js"></script>
		<script type="text/javascript" src="Ajax.js"></script>
		<?php require('PHP/Functions.php');?>

	</head>
	<body onload="Load();">
		<div id="Loading">
			<div id="Spinner" ><img src="Images/spinner.gif" alt="Spinner"/></div>
		</div>
		<?php include('PHP/About.php');?>
		<div id="Container">
			<div id="Header">
				<div id="Header-Left">
					<?php include('PHP/Error.php');?>
				</div>
				<div id="Header-Right">
					<img src="Images/Title.gif" class="Header-img" alt="WebIndex"/>
				</div>
			</div>
			<div id="Menu">
				<?php include('PHP/Menu.php');?>
			</div>
			<div id="Main">
				<?php include('PHP/Main.php');?>
			</div>
			<div id="Footer">
				<div class="Footer-Center"><a href="http://validator.w3.org/check?uri=referer"><img src="http://www.w3.org/Icons/valid-xhtml11" alt="Valid XHTML 1.1" height="31" width="88" /></a></div>
				<div class="Footer-Left"><a href="#" onclick="AboutUs();">About us</a></div>
				<div class="Footer-Right">Copyright &copy; 2012</div>
			</div>
		</div>
		<div id="FooterIMG"></div>


	</body>
	
</html>
