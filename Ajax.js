function DeleteImg()
{
	var xmlhttp;
	if (window.XMLHttpRequest)
		xmlhttp=new XMLHttpRequest();
	else if (window.ActiveXObject)
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	else
		alert("Your browser does not support XMLHTTP!");
	
	xmlhttp.onreadystatechange=function()
	{
		if(xmlhttp.readyState == 4 && xmlhttp.status==200)
		{
			var response=xmlhttp.responseText; 
			window.location.replace('Index.php?msg='+response);
		}
	}
	
	var source = document.getElementById("Preview").src;
	source=source.substr(42);
	
  
	var d=new Date();
	var url= "PHP/DeleteIMG.php?foo="+d;
	xmlhttp.open("POST",url,true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send("source="+source);

}

function GetSearch()
{
	var xmlhttp;
	if (window.XMLHttpRequest)
		xmlhttp=new XMLHttpRequest();
	else if (window.ActiveXObject)
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	else
		alert("Your browser does not support XMLHTTP!");
	
	xmlhttp.onreadystatechange=function()
	{
		if(xmlhttp.readyState == 4 && xmlhttp.status==200)
		{
			var response=xmlhttp.responseText; 
			document.getElementById("SearchResult").innerHTML=response;
		}
	}
	var temp = document.URL;
	
	var start=temp.indexOf("query");
	var end = temp.indexOf("&",start);
	var search=temp.substring(start+6,end).replace(/#/g,"");
	
	var start=temp.indexOf("type");
	var end = temp.indexOf("&",start);
	var type=temp.substring(start+5,end).replace(/#/g,"");
	if(start!=-1)
		var type=temp.substring(start+5).replace(/#/g,"");
	else 
		type=-1;

	
	var start=temp.indexOf("page");
	if(start!=-1)
		var page=temp.substring(start+5).replace(/#/g,"");
	else 
		page=0;
	
	var d=new Date();
	var url= "PHP/GetSearch.php?foo="+d;
	xmlhttp.open("POST",url,true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send("search="+search+"&type="+type+"&page="+page);

}

function GetBusinessForm() 
{
	var xmlhttp;
	if (window.XMLHttpRequest)
		xmlhttp=new XMLHttpRequest();
	else if (window.ActiveXObject)
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	else
		alert("Your browser does not support XMLHTTP!");
	
	xmlhttp.onreadystatechange=function()
	{
		if(xmlhttp.readyState == 4 && xmlhttp.status==200)
		{
			var response=xmlhttp.responseText; 
			document.getElementById("MyBusinessForm").innerHTML=response;
		}
	}
	
	
  
	var d=new Date();
	var url= "PHP/GetBusinessForm.php?foo="+d;
	xmlhttp.open("GET",url,true);
	xmlhttp.send(null);
}

function GetThumbnails()
{
	var xmlhttp;
	if (window.XMLHttpRequest)
		xmlhttp=new XMLHttpRequest();
	else if (window.ActiveXObject)
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	else
		alert("Your browser does not support XMLHTTP!");
	
	xmlhttp.onreadystatechange=function()
	{
		if(xmlhttp.readyState == 4 && xmlhttp.status==200)
		{
			var response=xmlhttp.responseText; 
			document.getElementById("IMGThumbnails").innerHTML=response;
		}
	}
	
	var d=new Date();
	var url= "PHP/GetThumbnails.php?foo="+d;
	xmlhttp.open("GET",url,true);
	xmlhttp.send(null);
}

function GetBusiness(businesstitle) //Preview Business
{
	var xmlhttp;
	if (window.XMLHttpRequest)
		xmlhttp=new XMLHttpRequest();
	else if (window.ActiveXObject)
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	else
		alert("Your browser does not support XMLHTTP!");
	
	xmlhttp.onreadystatechange=function()
	{
		if(xmlhttp.readyState == 4 && xmlhttp.status==200)
		{
			var response=xmlhttp.responseText; 
			document.getElementById("PreviewBusiness").innerHTML=response;
		}
	}
	
	var transparent=document.getElementById("BusinessTransparent").style;
	var PreviewBusiness=document.getElementById("PreviewBusiness").style;
	transparent.display="block";
	PreviewBusiness.display="block";
	
	
	var d=new Date();
	var url= "PHP/GetPreviewBusiness.php?foo="+d;
	xmlhttp.open("POST",url,true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send("BusinessTitle="+businesstitle);	
}
