<?php
	session_start();
	//Import
	require('DB_params.php');
	$SecurityCode=$_POST['SecurityCode'];
	$UserName=$_POST['UserName'];
	$Password=$_POST['Password'];
	$eMail=$_POST['eMail'];
	$result=true;
	$status='Successfully Registered';

	//Check for problems	
	$SecurityImage=$_SESSION['security_code'];
	if( $SecurityImage == $SecurityCode && !empty($SecurityImage) )//Check captcha
		{unset($_SESSION['security_code']);}
	else 
		{$status = 'Sorry, you have provided an invalid security code';
		$result=false;}
	
	if ($UserName == $eMail){$status = 'wrong username'; $result=false;}//Check UserName
	
	else if(strlen($Password)<8)//Check password legnth
		{$status = 'Small Password';
			$result=false;
		}
	else if ($result!=false)//if all ok start check password
	{
		for($i=0; $i<strlen($Password); $i++)//Check password value to be in(0-9,a-z,A-Z and -)
		{
			if(ord($Password[$i])<45){$result = false;}
			else if(ord($Password[$i])>45 && ord($Password[$i])<48){$result=false;}
			else if(ord($Password[$i])>57 && ord($Password[$i])<65){$result=false;}
			else if(ord($Password[$i])>90 && ord($Password[$i])<97){$result=false;}
			else if(ord($Password[$i])>122){$result=false;}
		}
		if($result==false){$status = 'wrong password'; }
	}
	if($result!=false)//if all ok start check eMail
	{
		$ampersatPos = strpos($eMail,"@");
		$dotPos = strpos($eMail,".");
		$dotPosAfterAmpersat = strpos($eMail,".",$ampersatPos);
		if ($ampersatPos<=0) {$result = false;}
		if ($dotPos<0) {$result = false;}
		if ($dotPosAfterAmpersat-$ampersatPos==1) {$result = false;} 
		if ( strpos($eMail,".")==0  ||  strrpos($eMail,".")==strlen($eMail)-1 ){$result = false;}
		if($result==false){$status = 'wrong mail'; }
	}
	if ($result!=false) //Register on Database
	{
		//Encrypt Password
		$salt=rand(10000,99999);
		$encryptedPass = crypt($Password,$salt);
		
		//Number for user verification
		$VerifiedNum=rand(10000,99999);
		
		
		/*--------------------------------------------Uncomment to Send eMail----------------------------------------------------------*/
	
		$to = $eMail;
		$subject = 'test';
		$message = 'http://127.0.0.1/project4/PHP/Validate.php?VerifiedNum='.$VerifiedNum ;
		if (mail($to, $subject, $message)) 
			{echo '<p>Mail sent successfully</p>';}
		else
			{echo '<p>Mail could not be sent</p>';}
		exit();
	
		
		$message = 'http://127.0.0.1/project4/PHP/Validate.php?VerifiedNum='.$VerifiedNum;

		
		
		//Database
		try
		{
			$pdoObject = new PDO("mysql:host=$dbhost; dbname=$dbname;", $dbuser, $dbpass);
			
			$sql='SELECT UserName FROM User WHERE UserName=:UserName';
			$statement = $pdoObject->prepare($sql);
			$statement->execute( array(':UserName'=>$UserName) );
			$record=$statement->fetch();
			if($record['UserName'] != $UserName)
			{
				$sql='INSERT INTO User (UserName, Salt, Password, eMail, VerifiedNum) VALUES (:UserName, :Salt, :Password, :eMail, :VerifiedNum)';
				$statement = $pdoObject->prepare($sql);
				$myresult=$statement->execute( array(':UserName'=>$UserName, ':Salt'=>$salt, ':Password'=>$encryptedPass, ':eMail'=>$eMail, ':VerifiedNum'=>$VerifiedNum));
				$status=$message;
				if ( !$myresult ) { $status = 'Failed to execute sql query';  }
			}
			else { $status = 'Username already exists'; }
			
			$statement ->closeCursor();
			$pdoObject = null;
		}
		catch (PDOException $e)
		{
			$status = 'PDO Exception: '.$e->getMessage();
		}
	}
	echo header('Location: ../Index.php?msg='.$status);
?>