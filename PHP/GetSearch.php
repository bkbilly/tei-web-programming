<?php
	$search=$_POST['search'];
	$page=$_POST['page'];
	$type=$_POST['type'];
	$x=0;

	$i=0;
	require('DB_params.php');
	try
	{
		$pdoObject = new PDO("mysql:host=$dbhost; dbname=$dbname;", $dbuser, $dbpass);
		
		/*------Get Results & Number of them------*/
		if ($type==-1) //If we Don't giveBusinessType
		{
			$sql='SELECT COUNT(*) FROM Business WHERE (BusinessTitle LIKE "%'.$search.'%" OR KeyWords LIKE "%'.$search.'%" OR WebAddress LIKE "%'.$search.'%")';
			$statement = $pdoObject->query($sql);
			$record = $statement->fetch();
			
			$countpage = $record['COUNT(*)'];
			if(($countpage % 4) != 0)
				$countpage = (int)($countpage / 4)+1;
			else
				$countpage = (int)($countpage / 4);
			
			
			$sql='SELECT * FROM Business WHERE (BusinessTitle LIKE "%'.$search.'%" OR KeyWords LIKE "%'.$search.'%" OR WebAddress LIKE "%'.$search.'%") LIMIT '.$page.',4 ';
			$statement = $pdoObject->query($sql);
		}
		else //If we give BusinessType
		{
			$sql='SELECT COUNT(*) FROM Business WHERE (BusinessTitle LIKE "%'.$search.'%" OR KeyWords LIKE "%'.$search.'%" OR WebAddress LIKE "%'.$search.'%") AND Business_Type="'.$type.'"';
			$statement = $pdoObject->query($sql);
			$record = $statement->fetch();
			
			$countpage = $record['COUNT(*)'];
			if(($countpage % 4) != 0)
				$countpage = (int)($countpage / 4)+1;
			else
				$countpage = (int)($countpage / 4);
			
			
			$sql='SELECT * FROM Business WHERE (BusinessTitle LIKE "%'.$search.'%" OR KeyWords LIKE "%'.$search.'%" OR WebAddress LIKE "%'.$search.'%") AND Business_Type="'.$type.'" LIMIT '.$page.',4 ';
			$statement = $pdoObject->query($sql);
		}
		
		/*------Write the results on Main------*/
		while ( $record = $statement->fetch() ) 
		{
			$bus=$record["BusinessTitle"];
			echo ('<div class="ResultBusiness">
				<img id="'.$record["BusinessTitle"].'" src="Images/Uploaded/'.$record["Default_IMG"].'" onclick="GetBusiness(this.id)"/>
				<h3>'.$record["BusinessTitle"].'</h3>-'.$record["Business_Type"].'<br/>
				'.$record["Address"].'-'.$record["PostalCode"].'<br/>
				'.$record["PhoneNumber"].'<br/>
				<a href="http://'.$record["WebAddress"].'">'.$record["WebAddress"].'</a>
				</div><br/>');
		}	
		
		/*------Write Pages------*/
		echo ('<div id="PageNum">');
		for($i=0; $i<$countpage; $i++)
		{
			echo ('<a href="#" onclick="ChangePage('.$x.')">'.($i+1).'</a>  ');
			$x=$x+4;
			
		}
		echo('</div>');
		
		echo('<a href="PHP/GetXML.php?search='.$search.'&type='.$type.'"> Get Results to XML </a>');
		
		
		$statement ->closeCursor();
		$pdoObject = null;
	}
	catch (PDOException $e)
	{
		$status='PDO Exception: '.$e->getMessage();
	}
?>