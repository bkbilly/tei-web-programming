<?php
	/*--------------Business Thumbnails--------------*/
	session_start();
	require('DB_params.php');

	try 
	{
		$pdoObject = new PDO("mysql:host=$dbhost;dbname=$dbname;", $dbuser, $dbpass);   
		$sql = "SELECT Name FROM Image WHERE Business_Title=(SELECT BusinessTitle FROM Business WHERE User_Name=:UserName);";
		$statement = $pdoObject->prepare($sql);
		$statement->execute( array(':UserName'=>$_SESSION['UserName']) );
		
		
		while ( $record = $statement->fetch() )
		{
			$temp="'".$record["Name"]."'";
			echo '<br><a href="#" onclick="PreviewImg('.$temp.')" ><img src="Images/Uploaded/'.$record["Name"].'" width=80px alt="'.$record["Name"].'" /></a></br>';
		}
		
		$statement->closeCursor();
		$pdoObject=null;
	}
	catch (PDOException $e) 
	{
		$status='PDO Exception: '.$e->getMessage();
	} 
	
	
?>