<?php
	require('DB_params.php');
	if (!isset($_GET['search']))
		die('ERROR: Please provide a search.');
	$search=$_GET['search'];
	$type=$_GET['type'];


	try 
	{
		$pdoObject = new PDO("mysql:host=$dbhost;dbname=$dbname;", $dbuser, $dbpass);
	
		if($type==-1) //If we Don't give BusinessType
		{
			$sql = 'SELECT * FROM Business WHERE (BusinessTitle LIKE "%'.$search.'%" OR KeyWords LIKE "%'.$search.'%" OR WebAddress LIKE "%'.$search.'%")';
			$statement = $pdoObject->query($sql);
		}
		else //If we give BusinessType
		{
			$sql = 'SELECT * FROM Business WHERE (BusinessTitle LIKE "%'.$search.'%" OR KeyWords LIKE "%'.$search.'%" OR WebAddress LIKE "%'.$search.'%") AND Business_Type="'.$type.'"';
			$statement = $pdoObject->query($sql);
		}
		
		/*------Write XML------*/
		header("Content-Type: text/xml; charset=UTF-8");
		echo '<?xml version="1.0" encoding="UTF-8"?'.'>'."\r\n";
		echo '<!DOCTYPE Business [
		<!ELEMENT Business (BusinessID*)>
		<!ELEMENT BusinessID (BusinessTitle,BusinessType,Address,PostalCode,PhoneNumber,KeyWords,WebAddress,Description)>
		<!ATTLIST BusinessID id CDATA #REQUIRED>
		<!ELEMENT BusinessTitle (#PCDATA)>
		<!ELEMENT BusinessType (#PCDATA)>
		<!ELEMENT Address (#PCDATA)>
		<!ELEMENT PostalCode (#PCDATA)>
		<!ELEMENT PhoneNumber (#PCDATA)>
		<!ELEMENT KeyWords (#PCDATA)>
		<!ELEMENT WebAddress (#PCDATA)>
		<!ELEMENT Description (#PCDATA)>
		]>'."\r\n";
		//echo '<!DOCTYPE Business SYSTEM "MyDTD.dtd">'."\r\n";----->it doesn't work 
		echo '<Business>'."\r\n";
		while ( $record = $statement->fetch() ) 
		{
		  echo '<BusinessID id="'.$record['BusinessTitle'].'">'."\r\n";
		  echo '   	<BusinessTitle>'.$record['BusinessTitle'].'</BusinessTitle>'."\r\n";
		  echo '   	<BusinessType>'.$record['Business_Type'].'</BusinessType>'."\r\n";
		  echo '   	<Address>'.$record['Address'].'</Address>'."\r\n";
		  echo '   	<PostalCode>'.$record['PostalCode'].'</PostalCode>'."\r\n";
		  echo '   	<PhoneNumber>'.$record['PhoneNumber'].'</PhoneNumber>'."\r\n";
		  echo '   	<KeyWords>'.$record['KeyWords'].'</KeyWords>'."\r\n";
		  echo '   	<WebAddress>'.$record['WebAddress'].'</WebAddress>'."\r\n";
		  echo '	<Description>'.$record['Description'].'</Description>'."\r\n";
		  echo '</BusinessID>'."\r\n";
		}
		echo '</Business>';
		
		
		$statement->closeCursor();
		$pdoObject = null;  
  } 
	catch (PDOException $e) 
	{   
		die( "PDOException: ".$e->getMessage() ); 
	}
?>

