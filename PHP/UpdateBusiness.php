<?php 
	session_start();
	require('DB_params.php');
	$UserName = $_SESSION['UserName'];
	
	$BusinessTitle = $_POST['BusinessTitle'];
	$Address = $_POST['Address'];
	$PostalCode = $_POST['PostalCode'];
	$PhoneNumber = $_POST['PhoneNumber'];
	$BusinessType = $_POST['BusinessType'];
	$BusinessTypeNew= $_POST['BusinessTypeNew'];
	$KeyWords = $_POST['KeyWords'];
	$WebAddress = $_POST['WebAddress'];
	$Description = $_POST['Description'];
	$DefaultImage=$_POST['DefaultImage'];

	
	if(!$BusinessTypeNew=="")
		$BusinessType=$BusinessTypeNew;
	
	$UploadFile = $_FILES['UpImage']['tmp_name'];
	$UploadName = $_FILES['UpImage']['name'];
	$UploadType = $_FILES['UpImage']['type'];
	$UploadSize = $_FILES['UpImage']['size']/1024;
	
	$imgset=false;
	
	/*----------Insert img to File----------*/
	if ((($UploadType == "image/gif")|| ($UploadType == "image/jpeg")|| ($UploadType == "image/jpg") || ($UploadType == "image/png")) &&  ($UploadSize < 500))
	{
		move_uploaded_file($UploadFile, "../Images/Uploaded/" . $UploadName);
		$imgset=true;
	}
	else if($UploadFile==null or $UploadFile=="")
		$imgset=false;
	else
	{	
		$status="Error Uploading Img";
		echo header('Location: ../Index.php?msg='.$status);
		$imgset=false;
		exit();
	}
	
	try
	{
		$pdoObject = new PDO("mysql:host=$dbhost; dbname=$dbname;", $dbuser, $dbpass);
		
		/*-------- Insert new Business Type----------*/
		$sql='SELECT BusinessType  FROM BusinessType WHERE BusinessType=:BusinessType';
		$statement = $pdoObject->prepare($sql);
		$statement->execute( array(':BusinessType'=>$BusinessType) );
		$record=$statement->fetch();
		
		if( !isset($record['BusinessType']))
		{
			$sql='INSERT INTO BusinessType VALUES ( :BusinessType)';
			$statement = $pdoObject->prepare($sql);
			$statement->execute( array(':BusinessType'=>$BusinessType) );
		}
		
		
		/*----------Choose to Update or Insert Business----------*/
		$sql='SELECT BusinessTitle  FROM Business WHERE User_Name=:UserName';
		$statement = $pdoObject->prepare($sql);
		$statement->execute( array(':UserName'=>$UserName) );
		$record=$statement->fetch();
		
		if( !isset($record['BusinessTitle']))
		{
			$sql='INSERT INTO Business ( BusinessTitle, User_Name, Business_Type, Address, PostalCode, PhoneNumber, KeyWords, WebAddress, Description ) VALUES ( :BusinessTitle, :UserName, :BusinessType, :Address, :PostalCode, :PhoneNumber, :KeyWords, :WebAddress, :Description )';
			$statement = $pdoObject->prepare($sql);
			$statement->execute( array(':BusinessTitle'=>$BusinessTitle,':UserName'=>$UserName,':BusinessType'=>$BusinessType,':Address'=>$Address,':PostalCode'=>$PostalCode,':PhoneNumber'=>$PhoneNumber,':KeyWords'=>$KeyWords,':WebAddress'=>$WebAddress,':Description'=>$Description) );
			$status= 'insert is done';
		}
		else
		{
			$sql='UPDATE Business SET BusinessTitle=:BusinessTitle, Business_Type=:BusinessType, Address=:Address, PostalCode=:PostalCode, PhoneNumber=:PhoneNumber, KeyWords=:KeyWords, WebAddress=:WebAddress, Description=:Description WHERE User_Name=:UserName ';
			$statement = $pdoObject->prepare($sql);
			$statement->execute( array(':BusinessTitle'=>$BusinessTitle,':BusinessType'=>$BusinessType,':Address'=>$Address,':PostalCode'=>$PostalCode,':PhoneNumber'=>$PhoneNumber,':KeyWords'=>$KeyWords,':WebAddress'=>$WebAddress, ':Description'=>$Description, ':UserName'=>$UserName));
			$record=$statement->fetch();
			$status='update is done';
		}
		
		/*----------Choose to Update or Insert Img----------*/
		if($imgset==true)
		{
			$sql = "INSERT INTO Image (Name, ALT, Business_Title) VALUES (:UploadName, :UploadType, :BusinessTitle)";
			$statement = $pdoObject->prepare($sql);
			$statement->execute( array(':UploadName'=>$UploadName, ':UploadType'=>$UploadType, 'BusinessTitle'=>$BusinessTitle) );
			
			$sql = "UPDATE Business SET Default_IMG=:UploadName WHERE User_Name=:UserName";
			$statement = $pdoObject->prepare($sql);
			$statement->execute( array(':UploadName'=>$UploadName,':UserName'=>$UserName) );

		}
		else
		{
			$sql = "UPDATE Business SET Default_IMG=:DefaultImage WHERE User_Name=:UserName";
			$statement = $pdoObject->prepare($sql);
			$statement->execute( array(':DefaultImage'=>$DefaultImage,':UserName'=>$UserName));
		}
		
		$statement ->closeCursor();
		$pdoObject = null;
	}
	catch (PDOException $e)
	{
		$status='PDO Exception: '.$e->getMessage();
	}
	
	echo header('Location: ../Index.php?msg='.$status);
?>