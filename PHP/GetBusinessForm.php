<?php
	session_start();
	require('DB_params.php');
	
	try 
	{
	  
		$pdoObject = new PDO("mysql:host=$dbhost;dbname=$dbname;", $dbuser, $dbpass); 
		
		/*------Get Records from Database------*/
		$sql = "SELECT * FROM Business WHERE User_Name=:UserName";
		$statement = $pdoObject->prepare($sql);
		$statement->execute( array(':UserName'=>$_SESSION['UserName']) );
		$record = $statement->fetch();
		
		$BusinessTitle = $record['BusinessTitle'];
		$Address = $record['Address'];
		$PostalCode = $record['PostalCode'];
		$PhoneNumber = $record['PhoneNumber'];
		$KeyWords = $record['KeyWords'];
		$WebAddress = $record['WebAddress'];
		$Description = $record['Description'];
		
		/*------Get Default Value for BusinessType------*/
		$sql = "SELECT Business_Type FROM Business WHERE User_Name='".$_SESSION['UserName']."'";
		$statement = $pdoObject->query($sql);
		$Selected=$statement->fetch();
		
		/*------Write all BusinessType------*/
		$sql = "SELECT BusinessType FROM BusinessType";
		$statement = $pdoObject->query($sql);
		$typecount=0;
		while ( $record = $statement->fetch() )
		{
			if($record["BusinessType"]==$Selected["Business_Type"])
				$type[$typecount]= '<option selected>'.$record["BusinessType"].'</option>';
			else
				$type[$typecount]= '<option >'.$record["BusinessType"].'</option>';
			$typecount++;
		}
	
		/*------Get Default Image------*/
		$sql = "SELECT * FROM Business WHERE User_Name='".$_SESSION['UserName']."'";
		$statement = $pdoObject->query($sql);
		$record = $statement->fetch();
		$Defaultimg=$record["Default_IMG"];
		
		/*------Write All Images------*/
		$sql = "SELECT * FROM Image WHERE Business_Title=( SELECT  BusinessTitle FROM Business WHERE User_Name='".$_SESSION['UserName']."')";
		$statement = $pdoObject->query($sql);
		
		$countimg=0;
		while ( $record = $statement->fetch() )
		{
			if($Defaultimg==$record["Name"])
				$Image[$countimg]= '<option selected>'.$record["Name"].'</option>';
			else
				$Image[$countimg]= '<option>'.$record["Name"].'</option>';
			$countimg++;
		}
		
		
		
		$statement->closeCursor();
		$pdoObject=null;
	}
	catch (PDOException $e) 
	{
		$status='PDO Exception: '.$e->getMessage();
	}
	
	
	echo('
	<form name="Business" id="Business" action="PHP/UpdateBusiness.php" method="post" onsubmit="return validate_Business();" enctype="multipart/form-data">
		<fieldset>
			<legend>
				<a href="#" onclick="HideEverything();"><img src="Images/CloseButton.gif" alt="Close"/></a>
			</legend>
			<table>
				<tr>
					<div id="ImportIMG">
					<input type="file" name="UpImage" accept="image/*" />
					<select name="DefaultImage" id="DefaultImage">
					</div>
					');
					for($i=0; $i<$countimg; $i++)
						echo ($Image[$i]);
					echo ('"</select>
				</tr>
				<tr>
					<td class="right">Business Title:</td>
					<td><input type="text" name="BusinessTitle" id="BusinessTitle" size="35" maxlength="20" onkeyup="BusinessNull(id);" onblur="BusinessNull(id);"  value="'.$BusinessTitle.'" /></td>
				</tr>
				<tr>
					<td class="right">Address:</td>
					<td><input type="text" name="Address" id="Address" size="35" maxlength="20" value="'.$Address.'"/></td>
				</tr>
				<tr>
					<td class="right">Postal Code:</td>
					<td><input type="text" name="PostalCode" id="PostalCode" size="15" maxlength="5" onkeyup="Number(id);" value="'.$PostalCode.'"/></td>
				</tr>
				<tr>
					<td class="right">Phone Number:</td>
					<td><input type="text" name="PhoneNumber" id="PhoneNumber" size="20" maxlength="15" value="'.$PhoneNumber.'"/></td>
				</tr>
				<tr>
					<td class="right">Business Type:</td>
					<td>
						<select name="BusinessType" id="BusinessType">');
						for($i=0; $i<$typecount; $i++)
							echo ($type[$i]);
						echo('</select>
						<input type="text" name="BusinessTypeNew" id="BusinessTypeNew" size="20" maxlength="25"/>
						<a href="#" onclick="BusinessTypeSwap();"> swap</a>
					</td>
				</tr>
				<tr>
					<td class="right">KeyWords:</td>
					<td><input type="text" name="KeyWords" id="KeyWords" size="35" maxlength="45" value="'.$KeyWords.'"/></td>
				</tr>
				<tr>
					<td class="right">WebAddress:</td>
					<td><input type="text" name="WebAddress" id="WebAddress" size="20" maxlength="25" value="'.$WebAddress.'"/></td>
				</tr>
				<tr>
					<td class="right">Description</td>
					<td><textarea name="Description" id="Description" rows="10" cols="30" onkeyup="DescriptionLimit(id);">'.$Description.'</textarea></td>
				</tr>
				<tr><td>
					<input type="submit" name="submit" id="submit" value="Insert" class="input-black"/>
				</td></tr>
				

				
			</table>
		</fieldset>
	</form>');
?>