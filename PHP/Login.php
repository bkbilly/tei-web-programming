<?php
	session_start();
	//Import
	require('DB_params.php');
	$UserName=$_POST['LoginName'];
	$Password=$_POST['LPassword'];
	
	
	try
	{
		$pdoObject = new PDO("mysql:host=$dbhost; dbname=$dbname;", $dbuser, $dbpass);
		
		$sql='SELECT UserName, Salt, Password, VerifiedNum, Verified  FROM User WHERE UserName=:UserName';
		$statement = $pdoObject->prepare($sql);
		$statement->execute( array(':UserName'=>$UserName) );
		$record=$statement->fetch();
		
		$salt=$record['Salt'];
		$encryptedPass = crypt($Password,$salt);
		
		if(!$record)
			{$status='ERROR: UserName does not exist!';}
		
		else if($record['Verified']==false)
			{$status='ERROR: Activate your account!';}
		
		else if($encryptedPass!=$record['Password'])
			{$status='ERROR: Wrong Password!';}
		
		else if($_SESSION['UserName']==$UserName)
		{$status='Session Already Exists';}
		
		else
		{
			$_SESSION['UserName']=$UserName;
			$expire=time()+60*60*24*30;
			setcookie("UserName",$record['UserName'],$expire,'/');
			$status='Successful_Login';
		}
		
		$statement ->closeCursor();
		$pdoObject = null;
		
	}
	catch (PDOException $e)
	{
		$status='PDO Exception: '.$e->getMessage();
	}
	
	echo header('Location: ../Index.php?msg='.$status);
	
?>