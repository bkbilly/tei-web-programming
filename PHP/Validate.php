<?php
	/*			Validate Registered User and Autologin the first time				*/
	session_start();
	require('DB_params.php');
	$VerifiedNum=$_GET['VerifiedNum'];
	try
	{
		$pdoObject = new PDO("mysql:host=$dbhost; dbname=$dbname;", $dbuser, $dbpass);
		
		$sql='SELECT * FROM User WHERE VerifiedNum=:VerifiedNum';
		$statement = $pdoObject->prepare($sql);
		$statement->execute( array(':VerifiedNum'=>$VerifiedNum) );
		$record=$statement->fetch();
		
		
		if($record['Verified']==false && $record['VerifiedNum']==$VerifiedNum)
		{
			$sql='UPDATE User SET Verified=1 WHERE VerifiedNum=:VerifiedNum ';
			$statement = $pdoObject->prepare($sql);
			$statement->execute( array(':VerifiedNum'=>$VerifiedNum) );
			
			$_SESSION['UserName']=$record['UserName'];
			$expire=time()+60*60*24*30;
			setcookie("UserName",$record['UserName'],$expire,'/');
			$status='Successful_Login';
			
		}
		else
		{
			$status='You have already activeted your account!!!';
		}
		
		$statement ->closeCursor();
		$pdoObject = null;
	}
	catch (PDOException $e)
	{
		$status='PDO Exception: '.$e->getMessage();
	}
	
	echo header('Location: ../Index.php?msg='.$status);
?>