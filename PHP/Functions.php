<?php
	/*-------------Get BusinessType for Search-------------*/
	function load_type()
	{
		try 
		{
			require('DB_params.php');
			$pdoObject = new PDO("mysql:host=$dbhost;dbname=$dbname;", $dbuser, $dbpass);
			
			$sql = "SELECT Business_Type FROM Business";
			$statement = $pdoObject->query($sql);
			$Selected=$statement->fetch();
			
			
			$sql = "SELECT BusinessType FROM BusinessType";
			$statement = $pdoObject->query($sql);
			
			while ( $record = $statement->fetch() )
			{
				if($record["BusinessType"]==$Selected["Business_Type"])
					echo '<option selected="selected">'.$record["BusinessType"].'</option>';
				else
					echo '<option >'.$record["BusinessType"].'</option>';
			}
			
			$statement->closeCursor();
			$pdoObject=null;
		}
		catch (PDOException $e) 
		{
			$status='PDO Exception: '.$e->getMessage();
		} 
	}

	
?>

