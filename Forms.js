function validate_Business()
{
	var result=true;
	var phonenumber=Number("PhoneNumber");
	var postalcode=Number("PostalCode");
	var title=BusinessNull("BusinessTitle");
	if (title==false || phonenumber==false || postalcode==false)
		result=false;
		
	return result;
}
function BusinessNull(vallue)
{
	var result=true;
	var title = document.getElementById(vallue);
	
	if (title.value=="" )
	{
		result=false
		title.style.backgroundColor="red";
	}
	else
		title.style.backgroundColor="white";
	return result;
}
function ChooseCSS(style)
{
	setCookie("Style", style, 30);
	window.location.reload();
}
function setCookie(c_name,value,exdays)
{
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	document.cookie=c_name + "=" + c_value;
}


function DescriptionLimit(id)
{
	var textdesc= document.getElementById(id);
	if(textdesc.value.length>450)
		textdesc.style.backgroundColor="red";
	else
		textdesc.style.backgroundColor="white";
}
function Number(vallue) 
{
	var num = document.getElementById(vallue);
	var result=true
	var i;
	for(i=0; i<num.value.length; i++)
	{
		if((num.value.charCodeAt(i)<48) || (num.value.charCodeAt(i)>57))
			result=false;
	}
	if (result==false)
		num.style.backgroundColor="red";
	else
		num.style.backgroundColor="white";
	return result;
}

function AboutUs() //OurInfo
{
	var transparent=document.getElementById("BusinessTransparent").style;
	var about=document.getElementById("AboutOurSelfs").style;
	about.display="block";
	transparent.display="block";
}


function ChangePage(page) //Ajax Pages
{
	var temp = document.URL;
	
	var start=temp.indexOf("query");
	var end = temp.indexOf("&",start);
	var query=temp.substring(start+6,end).replace(/#/g,"");
	
	var start=temp.indexOf("type");
	var end = temp.indexOf("&",start);
	var type=temp.substring(start+5,end).replace(/#/g,"");
	
	if(start!=-1)
		window.location.replace('Index.php?query='+query+"&type="+type+"&page="+page);
	else
		window.location.replace('Index.php?query='+query+"&page="+page);

}


function ReplaceURL(page) //On Search button
{
	var check = document.getElementById("CheckType");
	var query = document.getElementById("SearchBar").value;
	if (check.checked==true)
	{
		var selected = document.getElementById("SearchType").selectedIndex;
		var type = document.getElementById("SearchType").options[selected].text;
		window.location.replace('Index.php?query='+query+"&type="+type+"&page="+page);
	}
	else
		window.location.replace('Index.php?query='+query+"&page="+page);
	
	
	return false;

}

function HideType() //Checkbox to search for BusinessType
{
	var type=document.getElementById("SearchType");
	if (type.style.display=="block")
	{
		type.style.display="none";
	}
	else
	{
		type.style.display="block";
	}
}


function BusinessTypeSwap() //BusinessForm switch from New BusinessType to select BusinessType
{
	var type=document.getElementById("BusinessType");
	var typenew=document.getElementById("BusinessTypeNew");
	if (type.style.display=="none")
	{
		type.style.display="block";
		typenew.style.display="none";
	}
	else
	{
		type.style.display="none";
		typenew.style.display="block";
	}
	typenew.value="";
}

var tempimg; //Necessary to change Thubnails
function PreviewImg(img) //When you click on Thumbnails
{
	var transparent=document.getElementById("BusinessTransparent").style;
	var businessform=document.getElementById("MyBusinessForm").style;
	var previewimage=document.getElementById("PreviewImage").style;
	var IMGThumbnails=document.getElementById("IMGThumbnails").style;
	var image = document.getElementById("Preview");
	
	if(previewimage.display=="block" && tempimg==img)
	{
		transparent.display="block";
		businessform.display="block";
		previewimage.display="none";
	}
	else
	{
		transparent.display="block";
		businessform.display="none";
		previewimage.display="block";
	}
	image.src="Images/Uploaded/"+img;
	tempimg=img;
}

function HideEverything() //When you click on the Transparent Div
{
	var transparent=document.getElementById("BusinessTransparent").style;
	var businessform=document.getElementById("MyBusinessForm").style;
	var previewimage=document.getElementById("PreviewImage").style;
	var IMGThumbnails=document.getElementById("IMGThumbnails").style;
	var previewbusiness=document.getElementById("PreviewBusiness").style;
	var about=document.getElementById("AboutOurSelfs").style;

	
	transparent.display="none";
	businessform.display="none";
	previewimage.display="none";
	IMGThumbnails.display="none";
	previewbusiness.display="none";
	about.display="none";
}


function BusinessForm() //My Business link to open Form
{
	GetBusinessForm(); //Call from Ajax BusinessForm
	GetThumbnails(); //Call from Ajax Thubnails
	var transparent=document.getElementById("BusinessTransparent").style;
	var businessform=document.getElementById("MyBusinessForm").style;
	var previewimage=document.getElementById("PreviewImage").style;
	var IMGThumbnails=document.getElementById("IMGThumbnails").style;
	
	
	transparent.display="block";
	businessform.display="block";
	previewimage.display="none";
	IMGThumbnails.display="block";
	
	
}

function SwitchForm(value) //Switch between Register-Login
{
	var value2;
	//create values
	if (value=="RegisterForm")
	{
		value="Register";
		value2="Login";
	}
	else
	{
		value="Login";
		value2="Register";
	}
	var state=document.getElementById(value+"Form").style;
	var state2=document.getElementById(value2+"Form").style;
	//Open-Close div
	if (state.display=="block")
	{
		state.display="none";
	}
	else if (state2.display=="block")
	{
		state.display="block";
		state2.display="none";
	}
	else
	{
		state.display="block";
	}
}
function reloadImg(id) //Reload Captcha
{
	var obj = document.getElementById(id);
	var src = obj.src;
	var date = new Date();
	obj.src = src + '?v=' + date.getTime();
	return false;
}
function check_username(name,addr) //Validate UserName
{
	result=true;
	if (document.getElementById(name).value == document.getElementById(addr).value){result=false;}
	if (result==false){	document.getElementById(name).style.backgroundColor="red";}
	return result;
}
function check_password(pass) //Validate Password
{
	var password = document.getElementById(pass);
	var result=true
	var i;
	if(password.value.length<8){result=false;}
	for(i=0; i<password.value.length; i++)
	{
		if(password.value.charCodeAt(i)<45){result=false;}
		else if(password.value.charCodeAt(i)>45 && password.value.charCodeAt(i)<48){result=false;}
		else if(password.value.charCodeAt(i)>57 && password.value.charCodeAt(i)<65){result=false;}
		else if(password.value.charCodeAt(i)>90 && password.value.charCodeAt(i)<97){result=false;}
		else if(password.value.charCodeAt(i)>122){result=false;}
	}
	if (result==false){	password.style.backgroundColor="red";}
	return result;
}
function looks_like_email(addr) //Validate e-Mail
{
	var result = true;
	var str = document.getElementById(addr).value;
	var ampersatPos = str.indexOf("@");
	var dotPos = str.indexOf(".");
	var dotPosAfterAmpersat = str.indexOf(".", ampersatPos);
	if (ampersatPos<=0) {result = false;}
	if (dotPos<0) {result = false;}
	if (dotPosAfterAmpersat-ampersatPos==1) {result = false;} 
	if ( str.indexOf(".")==0  ||  str.lastIndexOf(".")==str.length-1 ){ result = false;}
	if (result==false) {document.getElementById(addr).style.backgroundColor="red";}
	return result;
}
function bluur(id) //Write to Register, Login grey text
{
	if( document.getElementById(id).value =="" ) {
		document.getElementById(id).value = id;
		document.getElementById(id).style.color="grey";
	}
}
function cliick(id) //OnClick Delete text from Register, Login
{
	document.getElementById(id).style.color="black"
	document.getElementById(id).style.backgroundColor="white"
	if( document.getElementById(id).value == id ) {
		document.getElementById(id).value = "";
	}
}
function validate_register() //Validator on Register
{
	var result=true;
	
	if ( !check_username("UserName","eMail") ){result=false}
	if ( !looks_like_email("eMail") ){result=false;}
	if ( !check_password("Password") ){result=false}

	return result;
}
function validate_login() //Validator on Login
{
	var result=true;
	
	if ( !check_password("LPassword") ){result=false}

	return result;
}