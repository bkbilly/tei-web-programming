function Load()
{
	var UserName=getCookie("UserName");
	var x=document.getElementById("DivNotLoged").style;
	var y=document.getElementById("DivLoged").style;
	var divsearch=document.getElementById("Search");
	var changestyle=document.getElementById("StyleOptions");
	var loading=document.getElementById("Loading").style;
	var choosecss1=document.getElementById("ChangeCSS1");
	var choosecss2=document.getElementById("ChangeCSS2");

	var style=getCookie("Style");
	
	
	document.body.style.cursor = 'wait';
	
	if (style!=null)
	{
		changestyle.href=style;
	}
	else
	{
		changestyle.href="Style2.css";
	}
	if (style=="Style.css")
	{
		choosecss1.setAttribute("onclick", "ChooseCSS('Style2.css');");
		choosecss1.innerHTML="Stolen Template";
		choosecss2.setAttribute("onclick", "ChooseCSS('Style2.css');");
		choosecss2.innerHTML="Stolen Template";
	}
	else
	{
		choosecss1.setAttribute("onclick", "ChooseCSS('Style.css');");
		choosecss1.innerHTML="Our Own Template";
		choosecss2.setAttribute("onclick", "ChooseCSS('Style.css');");
		choosecss2.innerHTML="Our Own Template";

	}
	
	if (UserName!=null && UserName!="")
	{
		x.display="none";
		y.display="block";
		document.title="Welcome "+UserName;
	}
	else
	{
		x.display="block";
		y.display="none";
	}
	var t=setTimeout("document.getElementById('Header-Left').innerHTML='';",4000);
	
	var temp = document.URL;
	
	var start=temp.indexOf("query");
	var end = temp.indexOf("&",start);
	var query=temp.substring(start+6,end).replace(/#/g,"");
	if (start!=-1) //if query exists
	{
		GetSearch();
		divsearch.style.margin="0px auto 50px";

	}
	
	var start=temp.indexOf("type");
	var end = temp.indexOf("&",start);
	var type=temp.substring(start+5,end).replace(/#/g,"");
	
	
	if (start!=-1) //if type exists
	{
		var i;
		var x=document.getElementById("SearchType");
		for (i=0;i<x.length;i++)
		{
			if(x.options[i].text==type)
			{
				var ind=(x.options[i].index);
				break;
			}
		}
		document.getElementById("CheckType").checked=true;
		document.getElementById("SearchType").style.display="block";
		document.getElementById("SearchType").selectedIndex=ind;
	}
	document.body.style.cursor = 'auto';
	loading.display="none";

}



function getCookie(c_name)
{
var i,x,y,ARRcookies=document.cookie.split(";");
for (i=0;i<ARRcookies.length;i++)
  {
  x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  x=x.replace(/^\s+|\s+$/g,"");
  if (x==c_name)
    {
    return unescape(y);
    }
  }
}
