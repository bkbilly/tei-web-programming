# README #

Project for Technical College made by ***Basilis Koulis*** and ***Sakis Tzoganakos***

### Summary ###

This project was made for our class on Web Programming.

Each business can Register to the site and add their Business Information with the Logo of their business. Then every user can Search for any business he wants by a category or by keyword search so that he can see all the information he wants.

### How do I get set up? ###

* Install **Apache**, **PHP** and **MySQL**
* Optionally install **phpMyAdmin**
* Create a **User** with a **Password**
* Import the **database.sql** to MySQL
* Place the project folders to **/var/www/html**